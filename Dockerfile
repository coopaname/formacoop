# FROM alpine/git AS base
# ADD . /bitnami/wordpress/wp-content/plugins/opaga
# # ADD https://downloads.wordpress.org/plugin/daggerhart-openid-connect-generic.3.9.0.zip /bitnami/wordpress/wp-content/plugins/daggerhart-openid-connect-generic
# RUN git clone --depth 1 https://gitlab.com/coopaname/coopaga.git /bitnami/wordpress/wp-content/themes/coopaga/
# # install the PHP extensions we need
# # RUN apt-get update && apt-get install -y libpng12-dev libjpeg-dev && rm -rf /var/lib/apt/lists/* \
# # 	&& docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
# # 	&& docker-php-ext-install gd
# # RUN docker-php-ext-install mysqli
# ENV WORDPRESS_PLUGINS opaga
FROM bitnami/wordpress:latest
# COPY --from=base --chown=daemon /bitnami/wordpress/wp-content/plugins/opaga /bitnami/wordpress/wp-content/plugins/opaga
# COPY --from=base /bitnami/wordpress/wp-content/plugins/daggerhart-openid-connect-generic /bitnami/wordpress/wp-content/plugins/daggerhart-openid-connect-generic
# COPY --from=base --chown=daemon /bitnami/wordpress/wp-content/themes/coopaga /bitnami/wordpress/wp-content/themes/coopaga