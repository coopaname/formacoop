<?php
/*
 * page-gestion.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_gestion_content($params = array())
{
    global $wpof;
    $role = wpof_get_role(get_current_user_id());
    $page_gestion = "menu";
    
    if (!in_array($role, array("um_responsable", "admin")))
        return "";
    
    $html = "";
    
    if (!empty($params['subpage']))
    {
        $page_gestion = $params['subpage'];
        require_once(wpof_path . "/pages/gestion/".$page_gestion.".php");
        $fonction = "get_page_gestion_$page_gestion";
        $html .= $fonction();
    }
    
    return get_page_gestion_menu($page_gestion).$html;
}

function get_page_gestion_menu($selected)
{
    global $wpof;
    
    ob_start(); ?>
    
    <ul id="gestion_menu">
    <?php foreach($wpof->pages_gestion as $slug => $titre) : ?>
        <li><a <?php echo ($slug == $selected) ? 'class="selected"' : ''; ?> href="<?php echo home_url()."/".$wpof->url_gestion."/".$slug; ?>"><?php echo $titre; ?></a></li>
    <?php endforeach; ?>
    </ul>
    
    <?php return ob_get_clean();
}
