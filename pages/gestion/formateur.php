<?php
/*
 * pages/gestion/formateur.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_page_gestion_formateur()
{
    global $wpof;
    $html = '<h2 class="gestion">'.$wpof->pages_gestion['formateur'].'</h2>';
    
    $html .= '<h3>'.__('Ajoutez de nouveaux utilisateurs').'</h3>';
    $html .= get_add_compte_form();
    $html .= '<h3>'.__('Gérez les utilisateurs actuels').'</h3>';
    $html .= get_tableau_gestion_formateurs();
    
    return $html;
}

function get_tableau_gestion_formateurs()
{
    $role = wpof_get_role(get_current_user_id());
    $formateurs = get_formateurs();
    init_anime_formation();
    init_anime_session();
    ob_start();
    ?>
    <table class="gestion_formateurs opaga opaga2 datatable">
    <thead>
    <?php echo reset($formateurs)->get_formateur_control_head(); ?>
    </thead>
    
    <?php
    foreach($formateurs as $f)
        echo $f->get_formateur_control();
    ?>
    </table>
    <?php
    return ob_get_clean();
}

?>
