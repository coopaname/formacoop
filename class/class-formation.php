<?php
/*
 * class-formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class Formation
{
    public $titre = "";
    public $permalien = "";
    public $slug = "";
    public $bandeau_id = "-1"; // image mise en avant
    
    // tableau d'ID des formateurs
    public $formateur = array();
    
    public $presentation = "";
    public $public_cible = "";
    public $objectifs = "";
    public $prerequis = "";
    public $programme = "";
    public $materiel_pedagogique = "";
    public $ressources = "";
    
    public $duree = 0;
    public $nb_jour = 0;
    public $tarif = 0;
    public $quizpr = "";
    public $quizobj = "";
    public $specialite = "";
    public $id;
    public $date_modif;
    
    public $acces_public = 0;
    
    public function __construct($formation_id = -1)
    {
        global $wpof;
        
        if ($formation_id > 0)
        {
            $this->id = $formation_id;
            $data = get_post($formation_id);
            $meta = get_post_meta($formation_id);
            
            // date de dernière modif
            $datetime_modif = DateTime::createFromFormat("Y-m-d H:i:s", $data->post_modified);
            $this->date_modif = date_i18n("j F Y", $datetime_modif->getTimestamp());
            
            // infos issues du post
            $this->titre = $data->post_title;
            $this->permalien = get_the_permalink($formation_id);
            $this->slug = $data->post_name;
            
            // infos issues des meta données du post
            $this->formateur = get_post_meta($formation_id, "formateur", true); // permet de récupérer l'info sous forme de tableau
            if (!is_array($this->formateur))
                $this->formateur = array();
            
            // caractéristiques de la formation
            foreach($wpof->desc_formation->term as $k => $t)
            {
                if ($wpof->{"formation_".$k."_mode"} != 'force')
                    $this->$k = (isset($meta[$k][0])) ? $meta[$k][0] : "";
                else
                    $this->$k = $wpof->{"formation_".$k."_text"};
            }
            
            $this->duree = (isset($meta['duree'][0])) ? $meta['duree'][0] : "";
            $this->nb_jour = (isset($meta['nb_jour'][0])) ? $meta['nb_jour'][0] : "";
            $this->acces_public = (isset($meta['acces_public'][0])) ? $meta['acces_public'][0] : "";
            
            // Tarif
            $this->init_tarif($meta);
            
            $this->quizpr = (isset($meta['quizpr'][0])) ? $meta['quizpr'][0] : "";
            $this->quizpr_id = (isset($meta['quizpr_id'][0])) ? $meta['quizpr_id'][0] : "";
            $this->quizobj = (isset($meta['quizobj'][0])) ? $meta['quizobj'][0] : "";
            $this->quizobj_id = (isset($meta['quizobj_id'][0])) ? $meta['quizobj_id'][0] : "";
            $this->ressources = (isset($meta['ressources'][0])) ? $meta['ressources'][0] : "";
            
            $this->specialite = (isset($meta['specialite'][0])) ? $meta['specialite'][0] : "";
            
            if (has_post_thumbnail($formation_id))
                $this->bandeau_id = get_post_thumbnail_id($formation_id);
        }
        else
        {
            foreach($wpof->desc_formation->term as $k => $t)
            {
                if ($wpof->{"formation_".$k."_mode"} != 'force')
                    $this->$k = "";
                else
                    $this->$k = $wpof->{"formation_".$k."_text"};
            }
        }
    }
    
    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpof;
        if ($meta_value === null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
            
        if (in_array($meta_key, array_keys($wpof->desc_formation->term)))
        {
            wp_update_post(array('ID' => $this->id));
            update_post_meta($this->id, "timestamp_modif", time());
        }
        if ($meta_key == 'titre')
        {
            $slug = sanitize_title($meta_value);
            update_post_meta($this->id, "timestamp_modif", time());
            return wp_update_post(array('ID' => $this->id, 'post_title' => $meta_value, 'post_name' => $slug));
        }
        else
            return update_post_meta($this->id, $meta_key, $meta_value);
    }
    
    /*
     * Qui peut modifier cette formation ?
     * Renvoie true si
     * $user_id a le rôle responsable ou admin
     * $user_id fait partie des formateurs animant cette session ($this->formateur)
     * $this->formateur est vide (session non définie)
     */
    public function can_edit($user_id = -1)
    {
        global $wpof;
        
        if ($user_id == -1)
            $user_id = get_current_user_id();
        
        $role = wpof_get_role($user_id);
        $super_roles = array("admin", "um_responsable");
        
        if ($role == null)
            return false;
        
        if (!isset($this->formateur)
            || !is_array($this->formateur)
            || count($this->formateur) == 0
            || in_array($user_id, $this->formateur)
            || in_array($role, $super_roles))
            return true;
        
        return false;
    }
    
    /*
     * Initialisation des tarifs
     */
    private function init_tarif($meta)
    {
        global $wpof;
        
        foreach(array_keys($wpof->type_tarif_formation->term) as $type)
            foreach(array_keys($wpof->declinaison_tarif) as $variante)
            {
                $tarif_var = "tarif_{$type}_{$variante}";
                $this->$tarif_var = (isset($meta[$tarif_var][0])) ? (float) ($meta[$tarif_var][0]) : 0;
                log_add($tarif_var." = ".$this->$tarif_var);
            }
            
        if (isset($meta['tarif'][0]) && $this->tarif_base_inter_heure == 0)
            $this->tarif_base_inter_heure = (float) ($meta['tarif'][0]);
        
        if ($this->tarif_base_inter_heure == 0)
            $this->tarif_base_inter_heure = $wpof->tarif_inter;
    }
    
    /*
     * Calcul de tarif
     * $modif = dernier paramètre modifié dans le tryptique duree, heure, total (heure et total sont préfixés soit par inter_, soit par intra_)
     * $type = type de tarif : le tarif de base est toujours présent, d'autres tarifs peuvent être définis dans les options
     * 
     * La règle de calcul :
     * si modif == duree ou heure, on recalcule total
     * si modif == total, on recalcule heure
     * En aucun cas la durée ne peut être modifiée par un changement de tarif
     */
    public function calcul_tarif($modif, $type = 'base')
    {
        global $wpof;
        
        if ($this->duree == 0)
            return;
        
        $update_form = array();
        
        if ($modif == "duree")
        {
            foreach (array_keys($wpof->type_tarif_formation->term) as $type)
                foreach(array("inter", "intra") as $var)
                {
                    $tarif_total = "tarif_{$type}_{$var}_total";
                    $tarif_heure = "tarif_{$type}_{$var}_heure";
                    $this->update_meta($tarif_total, $this->$tarif_heure * $this->duree);
                    $update_form[$tarif_total] = $this->$tarif_total;
                }
            
        }
        else
        {
            $param = explode('_', $modif);
            $tarif_total = "tarif_{$type}_".$param[0]."_total";
            $tarif_heure = "tarif_{$type}_".$param[0]."_heure";
            
            if ($param[1] == 'total')
            {
                $this->update_meta($tarif_heure, $this->$tarif_total / $this->duree);
                $update_form[$tarif_heure] = $this->$tarif_heure;
            }
            else
            {
                $this->update_meta($tarif_total, $this->$tarif_heure * $this->duree);
                $update_form[$tarif_total] = $this->$tarif_total;
            }
        }
        
        return json_encode($update_form);
    }
    
    /*
     * Renvoie le formulaire de création/modification de la formation
     */
    public function get_edit_formation()
    {
        global $wpof, $post;
        ob_start(); ?>
        <div id="formation" class="id formation edit-data" data-id="<?php echo $this->id; ?>">
        <?php echo get_icone_aide("fiche_formation", __("Important : comment rédiger votre fiche formation")); ?> 
        <a href="<?php the_permalink(); ?>" target="_blank"><span class="icone-bouton"><span class="dashicons dashicons-visibility"></span> <?php _e("Aperçu"); ?></span></a>
        <h1 id="edit-titre"><?php echo get_input_jpost($this, "titre", array('input' => 'text', 'label' => __('Titre de la formation'))); ?></h1>
        
        <div class="formation-edit flexrow nowrap margin auto-width">
            <div>
                <?php
                    echo get_input_jpost($this, "acces_public", array('input' => 'checkbox', 'label' => __('Fiche publiée au catalogue')));
                    if (!isset($wpof->formateur))
                        init_term_list("formateur");
                    echo get_input_jpost($this, "formateur", array('select' => 'multiple', 'rows' => 10, 'label' => __("Équipe pédagogique"),  'postprocess' => 'check_my_formation'));
                    echo get_input_jpost($this, "specialite", array('select' => '', 'label' => __("Spécialité"), 'first' => __("Choisissez une spécialité, la plus précise possible")));
                    echo get_input_jpost($this, "duree", array('input' => 'number', 'step' => 0.25, 'label' => __('Durée prévue de la formation en heures'), 'postprocess' => 'update_tarif', 'ppargs' => array('ppmodif' => 'duree')));
                    
                    foreach($wpof->type_tarif_formation->term as $type_key => $type) : ?>
                        <table class="opaga tarif" data-type="<?php echo $type_key; ?>">
                        <tr>
                            <td class="center"><strong><?php echo $type->text; ?></strong> <?php echo get_icone_aide("formation_tarif_$type_key"); ?></td>
                            <th class="thin"><?php echo $wpof->declinaison_tarif['heure']; ?></th>
                            <th class="thin"><?php echo $wpof->declinaison_tarif['total']; ?></th>
                        </tr>
                        <tr>
                            <th><?php echo $wpof->declinaison_tarif['inter']; ?></th>
                            <td class="right"><?php echo get_input_jpost($this, "tarif_{$type_key}_inter_heure", array('input' => 'number', 'step' => 0.001, 'aide' => false, 'postprocess' => 'update_tarif', 'ppargs' => array('ppmodif' => 'inter_heure', 'pptype' => $type_key))); ?></td>
                            <td class="right"><?php echo get_input_jpost($this, "tarif_{$type_key}_inter_total", array('input' => 'number', 'step' => 0.01, 'aide' => false, 'postprocess' => 'update_tarif', 'ppargs' => array('ppmodif' => 'inter_total', 'pptype' => $type_key))); ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $wpof->declinaison_tarif['intra']; ?></th>
                            <td class="right"><?php echo get_input_jpost($this, "tarif_{$type_key}_intra_heure", array('input' => 'number', 'step' => 0.001, 'aide' => false, 'postprocess' => 'update_tarif', 'ppargs' => array('ppmodif' => 'intra_heure', 'pptype' => $type_key))); ?></td>
                            <td class="right"><?php echo get_input_jpost($this, "tarif_{$type_key}_intra_total", array('input' => 'number', 'step' => 0.01, 'aide' => false, 'postprocess' => 'update_tarif', 'ppargs' => array('ppmodif' => 'intra_total', 'pptype' => $type_key))); ?></td>
                        </tr>
                        </table>
                    <?php endforeach;
                    
                    ?>
            </div>
            <div class="desc-formation metadata notif-modif">
                <?php
                    foreach($wpof->desc_formation->term as $k => $t)
                    {
                        if ($wpof->{"formation_".$k."_mode"} != 'force')
                            echo get_input_jpost($this, $k, array('editor' => '', 'label' => $t->text));
                        else
                        {
                            ?>
                            <h3><?php echo $t->text; ?></h3>
                            <p><em><?php _e("Ce paramètre est fixé par votre responsable de formation et ne peut être modifié."); ?></em></p>
                            <p><?php echo $wpof->{"formation_".$k."_text"}; ?></p>
                            <?php
                        }
                    }
                ?>
                <p class="bouton submit enregistrer-formation-input"><?php _e("Enregistrer les informations"); ?></p>
            </div>
        </div>
        
        </div>
        <script>
        jQuery(document).ready(function($)
        {
            // désactiver le clic sur le titre dans ce cas précis
            //$("#edit-titre").addClass('edit-data');
            $("h1 a").remove();
            //$("h1#edit-titre").show();
        });
        </script>
        <?php
        return ob_get_clean();
    }

    /*
     * Modèle pour la vue publique de la fiche formation
     */
    public function get_public_content()
    {
        global $wpof;
        
        $sessions = get_formation_sessions(array('formation_id' => $this->id, 'quand' => 'futur'));
        ob_start();
        ?>
        
        <div class="formation-global">
            <div class="formation-side-content">
            <p><?php printf(__("Programme édité le %s"), $this->date_modif); ?></p>
            <dl>
            <?php if ($this->duree > 0) : ?>
            <dt><?php _e("Durée"); ?></dt>
                <dd><?php printf(__("%d heures"), $this->duree); ?></dd>
                <?php if ($this->nb_jour != "") echo "<dd>".$this->nb_jour."</dd>"; ?>

                <?php
                foreach($wpof->type_tarif_formation->term as $type_key => $type) :
                    $tarif_inter_t = "tarif_{$type_key}_inter_total";
                    $tarif_intra_t = "tarif_{$type_key}_intra_total";
                    $tarif_inter_h = "tarif_{$type_key}_inter_heure";
                    $tarif_intra_h = "tarif_{$type_key}_intra_heure";
                    
                    if ($this->$tarif_inter_t + $this->$tarif_intra_t > 0) : ?>
                    <dt><?php echo $type->text; ?></dt>
                        <?php if ($this->$tarif_inter_t > 0) : ?>
                            <dd title="<?php echo get_tarif_formation($this->$tarif_inter_h).' '.__('de l’heure') ?>"> <?php echo get_tarif_formation($this->$tarif_inter_t); ?> <br />
                            <?php echo $wpof->declinaison_tarif['inter']; ?></dd>
                        <?php endif; ?>
                        
                        <?php if ($this->$tarif_intra_t > 0) : ?>
                            <dd title="<?php echo get_tarif_formation($this->$tarif_intra_h).' '.__('de l’heure') ?>"><?php echo get_tarif_formation($this->$tarif_intra_t); ?><br />
                            <?php echo $wpof->declinaison_tarif['intra']; ?></dd>
                        <?php endif; ?>
                    <?php endif;
                endforeach;
                
                echo $wpof->of_exotva ? "<br />".$wpof->terms_exo_tva : "";
                ?>
            <?php endif; ?>

            <?php if (count($this->formateur) > 0) :?>
                <dt><?php _e("Équipe pédagogique"); ?></dt>
                    <dd><?php the_liste_formateur(array('only' => $this->formateur)); ?></dd>
            <?php endif; ?>
            
            <dt><?php _e("Prochaines sessions"); ?></dt>
            <dd>
            <?php
                if (null == $sessions)
                    echo "<p>".$wpof->terms_no_session."</p>";
                else
                {
                    ?>
                    <ul class="list list_date">
                    <?php
                    foreach($sessions as $s)
                    {
                        echo "<li><span class='dashicons dashicons-calendar'></span> <a href='{$s->permalien}'>{$s->dates_texte}</a></br>{$s->ville}</li>";
                    }
                    ?>
                    </ul>
                    <?php
                }
            ?>
            </dd>
                <div class="icone-bouton get-proposition-pdf">
                <a href="/?download=proposition&ci=<?php echo $this->id; ?>&c=<?php echo $wpof->doc_context->formation;?>"><span class="dashicons dashicons-pdf"></span><?php _e("Ce programme en PDF"); ?></a>
                </div>
                <div class="icone-bouton dynamic-dialog" data-function="premier_contact" data-formationid="<?php echo $this->id; ?>">
                    <span class="dashicons dashicons-email-alt"></span>
                    <?php _e("Demande de renseignements"); ?>
                </div>
            </div>

            <!-- Contenu long -->
            <div class="formation-content">
            <dl>
            <?php
            
                $desc_proposition = $wpof->desc_formation->get_group("proposition");
                foreach($desc_proposition->term as $k => $t)
                {
                    ?>
                    <dt><?php echo $t->text; ?></dt>
                    <dd><?php echo wpautop($this->$k); ?></dd>
                    <?php
                }
            ?>
            </dl>
            </div> <!-- .formation-content -->
        </div> <!-- .formation-global -->
        <?php    
        return ob_get_clean();
    }
    
    /*
     * Modèle pour la vue publique de la fiche formation
     */
    public function get_public_excerpt()
    {
        global $wpof;
        
        //$sessions = get_formation_sessions(array('formation_id' => $this->id, 'quand' => 'futur'));
        $desc_proposition = $wpof->desc_formation->get_group("proposition");
        return $this->presentation;
        //return apply_filters( 'get_the_excerpt', $this->presentation, get_post($this->id) );
    }

    /*
     * Retourne le taux de complétion de la formation sous la forme :
     * nb_champs_remplis / nb_champs_a_repmlir
     */
    public function get_formation_completion($html = true)
    {
        global $wpof;
        
        $total = count($wpof->completion_formation);
        $rempli = 0;
        $vide = array();
        foreach($wpof->completion_formation as $k)
        {
            if (trim($this->$k) != "")
                $rempli++;
            elseif ($html)
                $vide[] = $k;
        }
        
        if ($html)
        {
            $class = ($rempli/$total == 1) ? "fait" : "attention";
            return '<span title="'.join('&#013;&#010;', $vide).'" class="'.$class.'">'.$rempli.'/'.$total.'</span>';
        }
        else
            return $rempli/$total;
    }
    
    public function get_formation_control_head()
    {
        global $wpof;
        $role = wpof_get_role(get_current_user_id());
        ob_start();
        ?>
        <tr>
        <th><?php _e("Titre formation"); ?></th>
        <?php if (in_array($role, $wpof->super_roles)) : ?>
        <th><?php _e("Formateur⋅trice(s)"); ?></th>
        <?php endif; ?>
        <th class='thin'><?php _e("Complétion description"); echo get_icone_aide("sessionformation_completion_description"); ?></th>
        <th class='thin'><?php _e("Visibilité catalogue"); echo get_icone_aide("formation_acces_public"); ?></th>
        <th class='thin'><?php _e("Programmer une session"); echo get_icone_aide("formation_programmer_session"); ?></th>
        </tr>
        <?php
        return ob_get_clean();
    }
    /*
     * Renvoie une ligne de tableau pour contrôler la formation
     * 
     */
    public function get_formation_control()
    {
        global $wpof;
        $role = wpof_get_role(get_current_user_id());
        ob_start();
        ?>
        <tr id="session<?php echo $this->id; ?>">
        <td>
        <a href="<?php echo get_edit_post_link($this->id); ?>"><span class="icone-bouton dashicons dashicons-edit"></span></a>
        <a href='<?php echo $this->permalien; ?>'><?php echo $this->titre; ?></a>
        </td>
        <?php if (in_array($role, $wpof->super_roles)) : ?>
        <td><?php the_liste_formateur(array('only' => $this->formateur)); ?></td>
        <?php endif; ?>
        <td class="center"><?php echo $this->get_formation_completion(true); ?></td>
        <td class="center">
        <?php echo get_input_jpost($this, "acces_public", array('input' => 'checkbox', 'display' => 'inline', 'aide' => false)); ?>
        </td>
        <td class="center">
        <p class="icone-bouton programme-session" data-formateur="<?php echo get_current_user_id(); ?>" data-id="<?php echo $this->id; ?>">
        <span class="dashicons dashicons-clock" ></span>
        </p></td>
        </tr>
        <?php
        return ob_get_clean();
    }
}

function get_formation_by_id($id)
{
    global $Formation;
    
    if (!isset($Formation[$id]))
        $Formation[$id] = new Formation($id);
        
    return $Formation[$id];
}

add_action( 'wp_ajax_formation_update_tarif', 'formation_update_tarif' );
function formation_update_tarif($args = array())
{
    $reponse = array();
    //$reponse['log'] = var_export($_POST, true);
    
    $formation = new Formation($_POST['formation_id']);
    $type = (isset($_POST['pptype'])) ? $_POST['pptype'] : "";
    $reponse['update_form'] = $formation->calcul_tarif($_POST['ppmodif'], $type);
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_add_new_formation', 'add_new_formation');
function add_new_formation()
{
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    if ($_POST['titre'] == "")
    {
        $reponse['erreur'] = __("Vous devez fournir un titre");
        echo json_encode($reponse);
        die();
    }
    $title = $_POST['titre'];
    
    $slug = sanitize_title($title);
    $formation_id = wp_insert_post(array('post_title' => $title, 'post_type' => 'formation', 'post_status' => 'publish', 'post_author' => get_current_user_id(), 'post_name' => $slug), true);
    if (is_wp_error($formation_id))
    {
        $reponse['erreur'] = __("Erreur de création de formation")." : ".__("WP Error : ").$formation_id->get_error_message();
        echo json_encode($reponse);
        die();
    }

    // Accès privé par défaut
    update_post_meta($formation_id, "acces_public", 0);
    
    if (!empty($_POST['formateur']))
    {
        // liste des formateurs sous forme de tableau
        $formateur = explode(',', $_POST['formateur']);
    }
    else
        $formateur = array(get_current_user_id());
    
    // ID des formateurs dans la formation
    update_post_meta($formation_id, "formateur", $formateur);

    $reponse['url'] = get_permalink($formation_id);
    echo json_encode($reponse);
    
    die();
}

add_filter('the_title', 'formation_edit_remove_title');
function formation_edit_remove_title($title)
{
    global $post, $wpof;
    
    if ($post && $post->post_type == "formation" && isset($_GET[$wpof->formation_edit_link_suffix]))
        $title = "";
    return $title;
}
