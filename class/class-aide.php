<?php
/*
 * class-aide.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class Aide
{
    public $titre = "";
    public $texte = "";
    public $slug = "";
    
    public function __construct($slug)
    {
        global $wpof;
        
        $this->slug = $slug;
        
        if (isset($wpof->aide->$slug))
            foreach(array("titre", "texte") as $f)
                $this->$f = $wpof->aide->$slug->$f;
    }
    
    public function init_defaut()
    {
        global $wpof;
        
        $json_file = $wpof->aide_file . ".json";
        
        $aide_data = file_get_contents(wpof_path . $json_file);
            
        $aide_data = json_decode($aide_data);
        
        if (isset($aide_data->{$this->slug}))
        {
            $aide = $aide_data->{$this->slug};
            $this->titre = $aide->titre;
            $this->texte = $aide->texte;
        }
    }
    
    // Supprime l'aide dans le JSON et dans la base de données locale
    public function delete_hard()
    {
        global $wpof;
        $message = array();
        
        $json_file = $wpof->aide_file . ".json";
        
        $aide_data = file_get_contents(wpof_path . $json_file);
        $aide_data = json_decode($aide_data);
        if (isset($aide_data->{$this->slug}))
        {
            unset($wpof->aide->{$this->slug});
            $wpof->export_aide();
            $message[] = "json ";
        }
        if (get_option("wpof_aide_".$this->slug))
        {
            delete_option("wpof_aide_".$this->slug);
            $message[] = "db";
        }
        return join(",", $message);
    }
    
    public function update($titre = null, $texte = null)
    {
        if ($titre)
            $this->titre = $titre;
        if ($texte)
            $this->texte = $texte;
        return update_option("wpof_aide_".$this->slug, json_encode($this, JSON_UNESCAPED_UNICODE));
    }
    
    public function get_icone($texte_icone = "")
    {
        $role = wpof_get_role(get_current_user_id());
        
        $couleur_icone = ($this->titre . $this->texte != "") ? "purple" : "grey";
        
        ob_start();
        if (in_array($role, array("admin", "um_responsable")) || $this->titre . $this->texte != "") :
        ?>
        <div class='icone-aide <?php echo $couleur_icone; ?> dynamic-dialog <?php echo ($texte_icone != "") ? "border" : ""; ?>' data-help='<?php echo $this->slug; ?>' data-function='help_dialog'>
        <span class='dashicons dashicons-editor-help'></span> <?php echo $texte_icone; ?>
        </div>
        <?php
        endif;
        
        return ob_get_clean();
    }
    
    public function get_aide($editable = true)
    {
        $role = wpof_get_role(get_current_user_id());
        
        ob_start();
        ?>
        <input type='hidden' name='slug' value='<?php echo $this->slug; ?>' />
        
        <?php if ($editable && in_array($role, array("um_responsable", "admin"))) : ?>
        <div class='icone-bouton aide-dialog-button aide_toggle_edit'>
        <span class='dashicons dashicons-edit'></span> <span class='aide_show'><?php _e("Modifier"); ?></span><span class='aide_edit'><?php _e("Afficher"); ?></span>
        </div>
        <div class="aide_edit">
        <div class='icone-bouton aide-dialog-button aide_reset'><span class='dashicons dashicons-undo'></span> <?php _e("Réinitialiser"); ?></div>
        <label class='top' for="<?php echo $this->slug; ?>_titre"><?php _e("Titre"); ?></label>
        <input type="text" name="titre" size="50" value="<?php echo $this->titre; ?>" id="<?php echo $this->slug; ?>_titre" />
        <label class='top' for="<?php echo $this->slug; ?>_texte"><?php _e("Texte"); ?></label>
        <?php wp_editor($this->texte, "texte"); ?>
        </div>
        
        <?php endif; ?>
        
        <div class="aide_show">
        <h2><?php echo $this->titre; ?></h2>
        <div><?php echo wpautop($this->texte); ?></div>
        </div>
        
        <?php
        return ob_get_clean();
    }
}

function get_icone_aide($slug = null, $texte_icone = "")
{
    if ($slug)
    {
        $aide = new Aide($slug);
        if (($icone = $aide->get_icone($texte_icone)) != "")
            return $icone;
    }
}

function get_aide($slug, $field = "texte")
{
    global $wpof;
    if (isset($wpof->aide->$slug) && isset($wpof->aide->$slug->$field))
        return $wpof->aide->$slug->$field;
    else
        return "";
}

add_action('wp_ajax_show_help', 'show_help');
function show_help()
{
    $reponse = "";
    
    if (isset($_POST['slug']))
    {
        $aide = new Aide($_POST['slug']);
        $reponse = $aide->get_aide();
    }
    else
        $reponse = "<div class='aide'><span class='erreur'>".__("Identifiant inconnu")." $slug</span></div>";
    
    echo $reponse;
    
    die();
}

add_action('wp_ajax_update_aide', 'update_aide');
function update_aide()
{
    $reponse = array();
    $aide = new Aide($_POST['slug']);
    if (!$aide->update(wp_unslash($_POST['titre']), wp_unslash($_POST['texte'])))
        $reponse['erreur'] = "Échec";

    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_get_aide_defaut', 'get_aide_defaut');
function get_aide_defaut()
{
    $reponse = array();
    
    $aide = new Aide($_POST['slug']);
    $aide->init_defaut();
    $reponse['titre'] = $aide->titre;
    $reponse['texte'] = $aide->texte;
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_export_aide', 'export_aide');
function export_aide()
{
    global $wpof;
    $reponse = array();
    
    if (isset($_POST['param']))
        $reponse['log'] = $wpof->export_aide($_POST['param']);
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_reset_aide', 'reset_aide');
function reset_aide()
{
    global $wpdb;
    $reponse = array();
    
    $query = "DELETE FROM {$wpdb->prefix}options WHERE option_name LIKE 'wpof_aide%';";
    $reponse['log'] = $query;
    $reponse['log'] .= $wpdb->query($query);
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_delete_aide', 'delete_aide');
function delete_aide()
{
    global $wpdb;
    
    if (isset($_POST['slug']))
    {
        $aide = new Aide($_POST['slug']);
        echo $aide->delete_hard();
    }
    
    die();
}

?>
